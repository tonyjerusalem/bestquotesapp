//
// QuoteCell
// BestQuotes
//

#import "QuoteCell.h"
#import "QuoteAccessor.h"
#import "Author.h"
#import "UIView+FrameControl.h"
#import "BQAppearance.h"

const CGFloat delta = 15;
const CGFloat defaultLabelHeight = 120;
const CGFloat defaultCellHeight = 190;


@interface QuoteCell ()

@property(weak, nonatomic) IBOutlet UIView *authorContainerView;
@property(weak, nonatomic) IBOutlet UIView *actionsContainerView;

@property(weak, nonatomic) IBOutlet UIImageView *authorImageView;
@property(weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property(weak, nonatomic) IBOutlet UILabel *quoteLabel;
@property(nonatomic, weak) IBOutlet UIButton *favouritesButton;
@property(nonatomic, weak) IBOutlet UIButton *shareButton;

@end

@implementation QuoteCell

+ (CGFloat)cellHeightWithText:(NSString *)text andWidth:(CGFloat)width expanded:(BOOL)expanded {
  if (!expanded) {
    return defaultCellHeight;
  }

  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, defaultLabelHeight)];
  label.lineBreakMode = NSLineBreakByWordWrapping;
  label.numberOfLines = 0;
  label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
  label.text = text;

  CGFloat labelHeight =
    label.expectedHeight;
  return labelHeight < defaultLabelHeight ?
    defaultCellHeight :
    (labelHeight - defaultLabelHeight) + defaultCellHeight + 20;
}

+ (void)sizeLabel:(UILabel *)label toFitAttributedText:(NSAttributedString *)text {
  CGFloat newHeight = label.expectedHeight;
  if (newHeight > defaultCellHeight) {
    [label resizeToStretch];
  }
}

- (IBAction)share:(id)sender {
  [self.delegate shareQuoteFromCell:self];
}

- (IBAction)favorites:(id)sender {
  [self.delegate favoriteQuoteFromCell:self];
}

- (IBAction)showAuthor:(id)sender {
  [self.delegate showAuthorFromCell:self];
}

- (void)configureCellWith:(id <QuoteAccessor>)quote expanded:(BOOL)expanded {
  self.authorContainerView.backgroundColor = [BQAppearance quoteCellHeaderColor];

  self.authorImageView.image = [UIImage imageWithData:quote.quoteAuthor.picture];

  self.authorImageView.layer.cornerRadius = 20.f;
  self.authorImageView.layer.borderWidth = 1;
  self.authorImageView.layer.borderColor = [UIColor clearColor].CGColor;

  NSMutableAttributedString *attributedString =
    [[NSMutableAttributedString alloc] initWithString:quote.quoteText attributes:@{
      NSFontAttributeName : self.quoteLabel.font
    }];
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineBreakMode =
    expanded ? NSLineBreakByWordWrapping : NSLineBreakByTruncatingTail;
  [attributedString addAttribute:NSParagraphStyleAttributeName
    value:paragraphStyle
    range:NSMakeRange(0, attributedString.length)];
  self.expanded = expanded;
  if (self.expanded) {
    self.quoteLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [QuoteCell sizeLabel:self.quoteLabel toFitAttributedText:attributedString];
  } else {
    self.quoteLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.quoteLabel modifyBounds:^(CGRect *bounds) {
      bounds->size.height = defaultLabelHeight;
    }];
  }
  self.quoteLabel.attributedText = attributedString;

  self.authorNameLabel.text = quote.quoteAuthor.name;

  if (quote.quotePostDate) { // means it comes from VK
    UIImage *image = quote.isQuoteLike ?
      [UIImage imageNamed:@"like-pressed-75.png"] :
        [UIImage imageNamed:@"like-75.png"];
    [self.favouritesButton setImage:image
      forState:UIControlStateNormal];
  } else {
    UIImage *image = quote.isQuoteLike ?
      [UIImage imageNamed:@"star-pressed-75.png"] :
      [UIImage imageNamed:@"star-75.png"];
    [self.favouritesButton setImage:image
      forState:UIControlStateNormal];
  }
}

@end