//
//  VKQuote.m
//  BestQuotes
//
//  Created by Anton Serov on 25/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "VKQuote.h"
#import "Author.h"
#import "Likes.h"


@implementation VKQuote

@dynamic postDate;
@dynamic postId;
@dynamic text;
@dynamic ownerId;
@dynamic author;
@dynamic like;

@end
