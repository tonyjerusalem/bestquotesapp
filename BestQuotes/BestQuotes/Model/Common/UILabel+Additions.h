//
// UILabel(Additions)
// Stampede
//

#import <Foundation/Foundation.h>

@interface UILabel (Additions)

- (float)expectedHeight;

- (float)expectedWidth;

- (void)resizeToStretch;

- (void)resizeToFill;

@end