//
//  Settings.m
//  BestQuotes
//
//  Created by Anton Serov on 24/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Settings.h"


@implementation Settings

@dynamic cacheQuotes;
@dynamic quotesPerRequest;
@dynamic userName;

@end
