//
// Created by Anton Serov on 20/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Quote+Additions.h"
#import "Author.h"
#import "DataManager.h"


@implementation Quote (Additions)


#pragma mark - QuoteAccessor impl


- (NSString *)quoteText {
  return self.text;
}

- (NSString *)formattedQuoteText {
  if (!self.author) {
    return self.quoteText;
  } else {
    NSString *ft = [NSString stringWithFormat:@"%@\n%@", self.quoteText, self.author.name];
    return ft;
  }
}

- (BOOL)isQuoteLike {
  return self.favorite.boolValue;
}

- (Author *)quoteAuthor {
  return self.author;
}


- (NSDate *)quotePostDate {
  return nil;
}

- (void)makeLike:(void (^)(void))completion {
  self.favorite = @(!self.favorite.boolValue);
  [[DataManager sharedManager] save];
  if (completion) {
    completion();
  }
}


@end