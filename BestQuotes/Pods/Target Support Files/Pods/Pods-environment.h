
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK 4
#define COCOAPODS_VERSION_MINOR_FlurrySDK 4
#define COCOAPODS_VERSION_PATCH_FlurrySDK 0

// FlurrySDK/FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK_FlurrySDK 4
#define COCOAPODS_VERSION_MINOR_FlurrySDK_FlurrySDK 4
#define COCOAPODS_VERSION_PATCH_FlurrySDK_FlurrySDK 0

// ISO8601DateFormatterValueTransformer
#define COCOAPODS_POD_AVAILABLE_ISO8601DateFormatterValueTransformer
#define COCOAPODS_VERSION_MAJOR_ISO8601DateFormatterValueTransformer 0
#define COCOAPODS_VERSION_MINOR_ISO8601DateFormatterValueTransformer 6
#define COCOAPODS_VERSION_PATCH_ISO8601DateFormatterValueTransformer 0

// MRProgress
#define COCOAPODS_POD_AVAILABLE_MRProgress
#define COCOAPODS_VERSION_MAJOR_MRProgress 0
#define COCOAPODS_VERSION_MINOR_MRProgress 8
#define COCOAPODS_VERSION_PATCH_MRProgress 0

// MRProgress/ActivityIndicator
#define COCOAPODS_POD_AVAILABLE_MRProgress_ActivityIndicator
#define COCOAPODS_VERSION_MAJOR_MRProgress_ActivityIndicator 0
#define COCOAPODS_VERSION_MINOR_MRProgress_ActivityIndicator 8
#define COCOAPODS_VERSION_PATCH_MRProgress_ActivityIndicator 0

// MRProgress/Blur
#define COCOAPODS_POD_AVAILABLE_MRProgress_Blur
#define COCOAPODS_VERSION_MAJOR_MRProgress_Blur 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Blur 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Blur 0

// MRProgress/Circular
#define COCOAPODS_POD_AVAILABLE_MRProgress_Circular
#define COCOAPODS_VERSION_MAJOR_MRProgress_Circular 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Circular 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Circular 0

// MRProgress/Helper
#define COCOAPODS_POD_AVAILABLE_MRProgress_Helper
#define COCOAPODS_VERSION_MAJOR_MRProgress_Helper 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Helper 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Helper 0

// MRProgress/Icons
#define COCOAPODS_POD_AVAILABLE_MRProgress_Icons
#define COCOAPODS_VERSION_MAJOR_MRProgress_Icons 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Icons 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Icons 0

// MRProgress/NavigationBarProgress
#define COCOAPODS_POD_AVAILABLE_MRProgress_NavigationBarProgress
#define COCOAPODS_VERSION_MAJOR_MRProgress_NavigationBarProgress 0
#define COCOAPODS_VERSION_MINOR_MRProgress_NavigationBarProgress 8
#define COCOAPODS_VERSION_PATCH_MRProgress_NavigationBarProgress 0

// MRProgress/Overlay
#define COCOAPODS_POD_AVAILABLE_MRProgress_Overlay
#define COCOAPODS_VERSION_MAJOR_MRProgress_Overlay 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Overlay 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Overlay 0

// MRProgress/ProgressBaseClass
#define COCOAPODS_POD_AVAILABLE_MRProgress_ProgressBaseClass
#define COCOAPODS_VERSION_MAJOR_MRProgress_ProgressBaseClass 0
#define COCOAPODS_VERSION_MINOR_MRProgress_ProgressBaseClass 8
#define COCOAPODS_VERSION_PATCH_MRProgress_ProgressBaseClass 0

// MRProgress/Stopable
#define COCOAPODS_POD_AVAILABLE_MRProgress_Stopable
#define COCOAPODS_VERSION_MAJOR_MRProgress_Stopable 0
#define COCOAPODS_VERSION_MINOR_MRProgress_Stopable 8
#define COCOAPODS_VERSION_PATCH_MRProgress_Stopable 0

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// MagicalRecord/Core
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Core
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Core 0

// RKValueTransformers
#define COCOAPODS_POD_AVAILABLE_RKValueTransformers
#define COCOAPODS_VERSION_MAJOR_RKValueTransformers 1
#define COCOAPODS_VERSION_MINOR_RKValueTransformers 1
#define COCOAPODS_VERSION_PATCH_RKValueTransformers 0

// RestKit
#define COCOAPODS_POD_AVAILABLE_RestKit
#define COCOAPODS_VERSION_MAJOR_RestKit 0
#define COCOAPODS_VERSION_MINOR_RestKit 23
#define COCOAPODS_VERSION_PATCH_RestKit 3

// RestKit/Core
#define COCOAPODS_POD_AVAILABLE_RestKit_Core
#define COCOAPODS_VERSION_MAJOR_RestKit_Core 0
#define COCOAPODS_VERSION_MINOR_RestKit_Core 23
#define COCOAPODS_VERSION_PATCH_RestKit_Core 3

// RestKit/CoreData
#define COCOAPODS_POD_AVAILABLE_RestKit_CoreData
#define COCOAPODS_VERSION_MAJOR_RestKit_CoreData 0
#define COCOAPODS_VERSION_MINOR_RestKit_CoreData 23
#define COCOAPODS_VERSION_PATCH_RestKit_CoreData 3

// RestKit/Network
#define COCOAPODS_POD_AVAILABLE_RestKit_Network
#define COCOAPODS_VERSION_MAJOR_RestKit_Network 0
#define COCOAPODS_VERSION_MINOR_RestKit_Network 23
#define COCOAPODS_VERSION_PATCH_RestKit_Network 3

// RestKit/ObjectMapping
#define COCOAPODS_POD_AVAILABLE_RestKit_ObjectMapping
#define COCOAPODS_VERSION_MAJOR_RestKit_ObjectMapping 0
#define COCOAPODS_VERSION_MINOR_RestKit_ObjectMapping 23
#define COCOAPODS_VERSION_PATCH_RestKit_ObjectMapping 3

// RestKit/Support
#define COCOAPODS_POD_AVAILABLE_RestKit_Support
#define COCOAPODS_VERSION_MAJOR_RestKit_Support 0
#define COCOAPODS_VERSION_MINOR_RestKit_Support 23
#define COCOAPODS_VERSION_PATCH_RestKit_Support 3

// SOCKit
#define COCOAPODS_POD_AVAILABLE_SOCKit
#define COCOAPODS_VERSION_MAJOR_SOCKit 1
#define COCOAPODS_VERSION_MINOR_SOCKit 1
#define COCOAPODS_VERSION_PATCH_SOCKit 0

// TransitionKit
#define COCOAPODS_POD_AVAILABLE_TransitionKit
#define COCOAPODS_VERSION_MAJOR_TransitionKit 2
#define COCOAPODS_VERSION_MINOR_TransitionKit 1
#define COCOAPODS_VERSION_PATCH_TransitionKit 0

// VK-ios-sdk
#define COCOAPODS_POD_AVAILABLE_VK_ios_sdk
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.0.8.6.

