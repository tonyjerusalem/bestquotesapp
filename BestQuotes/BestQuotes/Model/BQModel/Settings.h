//
//  Settings.h
//  BestQuotes
//
//  Created by Anton Serov on 24/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Settings : NSManagedObject

@property (nonatomic, retain) NSNumber * cacheQuotes;
@property (nonatomic, retain) NSNumber * quotesPerRequest;
@property (nonatomic, retain) NSString * userName;

@end
