//
// Created by Anton Serov on 21/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "AuthorVC.h"
#import "Author.h"

@interface AuthorVC ()

@property (nonatomic, weak) IBOutlet UIImageView *authorImageView;
@property (nonatomic, weak) IBOutlet UILabel *authorNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *authorLiveDatesLabel;
@property (nonatomic, weak) IBOutlet UITextView *authorInfoTextView;

@end

@implementation AuthorVC

#pragma mark - Appearance

- (void)updateViews {
  self.authorImageView.image = [UIImage imageWithData:self.author.picture];
  self.authorNameLabel.text = self.author.name;
  NSString *liveDates = nil;
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.dateFormat = @"DD.MM.YYYY";
  if (self.author.deathday == nil) {
    liveDates = [NSString stringWithFormat:@"%@ — н.в.",
        [dateFormatter stringFromDate:self.author.birthday]];
  } else {
    liveDates = [NSString stringWithFormat:@"%@ — %@",
        [dateFormatter stringFromDate:self.author.birthday],
        [dateFormatter stringFromDate:self.author.deathday]];
  }
  self.authorLiveDatesLabel.text = liveDates;
  self.authorInfoTextView.text = self.author.info;
}


#pragma mark - SetGet


- (void)setAuthor:(Author *)author {
  if (_author == author) {
    return;
  }

  _author = author;

  [self updateViews];
}


#pragma mark - Lifecycle


- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  [self updateViews];
}

@end