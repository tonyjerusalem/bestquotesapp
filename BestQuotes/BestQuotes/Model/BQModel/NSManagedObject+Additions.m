//
// Created by Anton Serov on 16/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "NSManagedObject+Additions.h"


@implementation NSManagedObject (Additions)

- (NSError *)errorFromOriginalError:(NSError *)originalError error:(NSError *)secondError {
  NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
  NSMutableArray *errors = [NSMutableArray arrayWithObject:secondError];

  if ([originalError code] == NSValidationMultipleErrorsError) {

    [userInfo addEntriesFromDictionary:[originalError userInfo]];
    [errors addObjectsFromArray:[userInfo objectForKey:NSDetailedErrorsKey]];
  }
  else {
    [errors addObject:originalError];
  }

  [userInfo setObject:errors forKey:NSDetailedErrorsKey];

  return [NSError errorWithDomain:NSCocoaErrorDomain
    code:NSValidationMultipleErrorsError
    userInfo:userInfo];
}

@end