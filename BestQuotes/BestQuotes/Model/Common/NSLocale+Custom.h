//
// NSLocale(Custom)
// BestQuotes
//

#import <Foundation/Foundation.h>

@interface NSLocale (Custom)

- (NSString *)shortLocaleIdentifier;

@end