//
//  QuotesVC.h
//  BestQuotes
//
//  Created by Anton Serov on 18.09.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableVC.h"

typedef NS_ENUM(NSInteger, DisplayQuoteType) {
  DisplayDBQuoteType = 0,
  DisplayVKQuoteType,
  DisplayFavQuoteType
};

@class QuotesTableView;
@class MRNavigationBarProgressView;

@interface QuotesVC : BaseTableVC

@property (nonatomic) DisplayQuoteType type;
@property (nonatomic, readonly) MRNavigationBarProgressView *navigationBarProgressView;

@end
