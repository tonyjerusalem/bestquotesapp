//
// Created by Anton Serov on 04/05/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "VKActivity.h"
#import "VKManager.h"
#import "MRProgressOverlayView.h"
#import "NSObject+Blocks.h"
#import "VKQuote.h"
#import "QuoteAccessor.h"
#import "VKQuote+Additions.h"

NSString *const BQActivityTypeVK = @"BQActivityTypeVK";

@implementation VKActivity

- (NSString *)activityType {
  return BQActivityTypeVK;
}

- (NSString *)activityTitle {
  return @"VKontakte";
}

- (UIImage *)activityImage {
  return [UIImage imageNamed:@"vk_activity"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
  return [VKManager sharedManager].authorized;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
//  for (id item in activityItems) {
//    if ([item isKindOfClass:[NSString class]]) {
//      self. =
//        [(self.shareText ? self.shareText : @"")
//          stringByAppendingFormat:@"%@%@",(self.shareText ? @" " : @""),item];
//    }
//  }
}

- (void)performActivity {
  MRProgressOverlayView *progressView =
    [MRProgressOverlayView showOverlayAddedTo:appDelegate.window animated:YES];
  progressView.titleLabelText = @"Posting to your timeline ...";
  progressView.mode = MRProgressOverlayViewModeIndeterminateSmall;
  __weak __typeof(self) weakSelf = self;
  if (self.quote.quotePostDate) {
    VKQuote *vkQuote = (VKQuote *)self.quote;
    [[VKManager sharedManager]
      repostOnWall:@""
      groupId:vkQuote.messageAddress
      completion:^(VKError *error) {
        CGFloat delay = 0.2f;
        NSString *message = @"Done!";

        if (error) {
          delay = 1.f;
          message = error.errorMessage;
        }

        progressView.titleLabelText = message;
        [[NSOperationQueue currentQueue] performAfterDelay:delay block:^{
          [progressView dismiss:YES];
        }];

        [weakSelf.controller dismissViewControllerAnimated:YES completion:nil];
    }];
  } else {
    [[VKManager sharedManager] postOnWall:self.quote.formattedQuoteText completion:^(VKError *error) {
      CGFloat delay = 0.2f;
      NSString *message = @"Done!";

      if (error) {
        delay = 1.f;
        message = error.errorMessage;
      }

      progressView.titleLabelText = message;
      [[NSOperationQueue currentQueue] performAfterDelay:delay block:^{
        [progressView dismiss:YES];
      }];

      [weakSelf.controller dismissViewControllerAnimated:YES completion:nil];
    }];
  }

}


@end