//
//  Quote.m
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Quote.h"


@implementation Quote

@dynamic favorite;
@dynamic text;
@dynamic author;

@end
