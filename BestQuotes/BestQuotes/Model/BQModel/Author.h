//
//  Author.h
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Quote, VKQuote;

@interface Author : NSManagedObject

@property(nonatomic, retain) NSDate *birthday;
@property(nonatomic, retain) NSDate *deathday;
@property(nonatomic, retain) NSString *info;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSData *picture;
@property(nonatomic, retain) NSSet *quotes;
@property(nonatomic, retain) NSSet *vkquotes;
@end

@interface Author (CoreDataGeneratedAccessors)

- (void)addQuotesObject:(Quote *)value;

- (void)removeQuotesObject:(Quote *)value;

- (void)addQuotes:(NSSet *)values;

- (void)removeQuotes:(NSSet *)values;

- (void)addVkquotesObject:(VKQuote *)value;

- (void)removeVkquotesObject:(VKQuote *)value;

- (void)addVkquotes:(NSSet *)values;

- (void)removeVkquotes:(NSSet *)values;

@end
