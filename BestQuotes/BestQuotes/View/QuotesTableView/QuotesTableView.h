//
// QuotesTableView
// BestQuotes
//

#import <Foundation/Foundation.h>

@interface QuotesTableView : UITableView

@property(nonatomic, strong) NSString *unavailableText;
@property(nonatomic, assign) BOOL availiable;

@end