//
// Created by Anton Serov on 21/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTableVC.h"

@class Author;


@interface AuthorVC : BaseTableVC

@property (nonatomic, weak) Author *author;

@end