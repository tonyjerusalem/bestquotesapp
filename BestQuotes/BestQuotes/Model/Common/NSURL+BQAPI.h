//
// Created by Anton Serov on 07/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (BQAPI)

+ (NSURL *)baseAPIURL;

@end