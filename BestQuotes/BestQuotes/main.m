//
//  main.m
//  BestQuotes
//
//  Created by Anton Serov on 21.08.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//

int main(int argc, char *argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
