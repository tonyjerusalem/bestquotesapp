//
// UILabel(Additions)
// Stampede
//

#import "UIView+FrameControl.h"

@implementation UILabel (Additions)

- (NSAttributedString *)expectedAttributedString {
  NSMutableAttributedString *text;
  if (self.attributedText) {
    text = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
  } else {
    NSMutableAttributedString *attributedString =
      [[NSMutableAttributedString alloc] initWithString:self.text attributes:@{
        NSFontAttributeName: self.font
      }];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    [attributedString addAttribute:NSParagraphStyleAttributeName
      value:paragraphStyle
      range:NSMakeRange(0, attributedString.length)];
  }
  return text;
}

- (void)resizeToStretch {
  CGFloat newHeight = [self expectedHeight];
  [self modifyBounds:^(CGRect *bounds) {
    bounds->size.height = newHeight;
  }];
}

- (void)resizeToFill {
  float height = [self expectedHeight];
  float width = [self expectedWidth];
  [self modifyBounds:^(CGRect *bounds) {
    bounds->size.height = height;
    bounds->size.width = width;
  }];
}

- (float)expectedHeight {
  const CGRect textRect =
    [self.expectedAttributedString
      boundingRectWithSize:
        CGSizeMake(CGRectGetWidth(self.frame), CGFLOAT_MAX)
      options:NSStringDrawingUsesLineFragmentOrigin
      context:nil];
  CGFloat newHeight = ceilf(textRect.size.height);
  return newHeight;
}

- (float)expectedWidth {
  const CGRect textRect =
    [self.expectedAttributedString
      boundingRectWithSize:
        CGSizeMake(CGFLOAT_MAX, CGRectGetHeight(self.frame))
      options:NSStringDrawingUsesLineFragmentOrigin
      context:nil];
  CGFloat newHeight = ceilf(textRect.size.width);
  return newHeight;
}

@end