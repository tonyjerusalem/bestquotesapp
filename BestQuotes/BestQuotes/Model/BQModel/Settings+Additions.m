//
// Created by Anton Serov on 24/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Settings+Additions.h"
#import "DataManager.h"


@implementation Settings (Additions)

+ (instancetype)sharedSettings {
  return [Settings MR_findFirst];
}

- (void)setupNameAndSave:(NSString *)name {
  Settings *settings = [Settings MR_findFirst];
  settings.userName = name;
  [[DataManager sharedManager] save];

}

@end