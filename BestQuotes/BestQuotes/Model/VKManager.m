//
// Created by Anton Serov on 23/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "VKManager.h"
#import "Settings.h"
#import "Settings+Additions.h"

NSString * const VKAppId = @"3917125";
NSString * const kDefaultsVKTokenKey = @"kDefaultsVKTokenKey";

@interface VKManager() <VKSdkDelegate, UIApplicationDelegate> {
  TypicalVKCompletionBlock _typicalVKCompletionBlock;
}

@property (nonatomic, strong) VKAccessToken *accessToken;

@end

@implementation VKManager

#pragma mark - Singltone

+ (instancetype)sharedManager {
  static VKManager *_sharedManager;
  if (_sharedManager) {
    return _sharedManager;
  }

  static dispatch_once_t pointer;
  dispatch_once(&pointer, ^{
    _sharedManager = [[VKManager alloc] init];
  });
  return _sharedManager;
}

#pragma mark - Initialize

- (instancetype)init {
  self = [super init];
  if (self) {
    [VKSdk initializeWithDelegate:self andAppId:VKAppId];
    if ([VKSdk wakeUpSession])  {
      _accessToken = [VKAccessToken tokenFromDefaults:kDefaultsVKTokenKey];
      if (_accessToken && !_accessToken.isExpired) {
        [VKSdk setAccessToken:_accessToken];
      } else if (_accessToken.isExpired) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:nil forKey:kDefaultsVKTokenKey];
        [defaults synchronize];
        [VKSdk setAccessToken:nil];
      }
    } else {
      [VKSdk setAccessToken:nil];
    }
  }
  return self;
}

#pragma mark - SetGet

- (BOOL)isAuthorized {
  return [VKSdk isLoggedIn];
}

#pragma mark - Client Actions

- (void)authorize:(TypicalVKCompletionBlock)completion {
  if (self.isAuthorized) {
    completion(nil);
    return;
  }
  _typicalVKCompletionBlock = [completion copy];
  NSArray *permissions = @[@"notify", @"friends", @"wall"];
  [VKSdk authorize:permissions];
}

- (void)signOut:(TypicalVKCompletionBlock)completion {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:nil forKey:kDefaultsVKTokenKey];
  [defaults synchronize];
  [VKSdk setAccessToken:nil];
  [[Settings sharedSettings] setupNameAndSave:nil];
  if (completion) {
    completion(nil);
  }
}

- (void)likeWithId:(NSString *)id ownerId:(NSString *)ownerId completion:(TypicalVKCompletionBlock)completion {
  VKRequest *likeRequest =
    [VKApi requestWithMethod:@"likes.add"
      andParameters:
        @{
          @"type": @"post",
          @"item_id": id,
          @"owner_id": ownerId
        }
      andHttpMethod:@"GET"];
  [self executeTypicalRequest:likeRequest completion:completion];
}

- (void)unlikeWithId:(NSString *)id ownerId:(NSString *)ownerId completion:(TypicalVKCompletionBlock)completion {
  VKRequest *likeRequest =
    [VKApi requestWithMethod:@"likes.delete"
      andParameters:
        @{
          @"type": @"post",
          @"item_id": id,
          @"owner_id": ownerId
        }
      andHttpMethod:@"GET"];
  [self executeTypicalRequest:likeRequest completion:completion];
}

- (void)postOnWall:(NSString *)text completion:(TypicalVKCompletionBlock)completion {
  VKRequest *postRequest =
    [VKApi requestWithMethod:@"wall.post"
      andParameters:
        @{
          @"message": text
        }
      andHttpMethod:@"GET"];
  [self executeTypicalRequest:postRequest completion:completion];
}

- (void)repostOnWall:(NSString *)text
             groupId:(NSString *)groupId
          completion:(TypicalVKCompletionBlock)completion {
  VKRequest *postRequest =
    [VKApi requestWithMethod:@"wall.repost"
      andParameters:
        @{
          @"message": text,
          @"object": groupId
        }
      andHttpMethod:@"GET"];
  [self executeTypicalRequest:postRequest completion:completion];
}

- (void)executeTypicalRequest:(VKRequest *)request completion:(TypicalVKCompletionBlock)completion {
  self->_typicalVKCompletionBlock = [completion copy];
  __weak __typeof(self) weakSelf = self;
  [request executeWithResultBlock:^(VKResponse *response) {
    __strong __typeof(weakSelf) strongSelf = weakSelf;
    if (!strongSelf) {
      return;
    }
    if (strongSelf->_typicalVKCompletionBlock) {
      strongSelf->_typicalVKCompletionBlock(nil);
    }
  }
    errorBlock:^(NSError *error) {
      NSLog(@"VK error: %@ while executing request: %@", error, request);
      __strong __typeof(weakSelf) strongSelf = weakSelf;
      if (!strongSelf) {
        return;
      }
      if (strongSelf->_typicalVKCompletionBlock) {
        strongSelf->_typicalVKCompletionBlock(error.vkError);
        strongSelf->_typicalVKCompletionBlock = NULL;
      }
    }];
}

#pragma mark - VKSdkDelegate impl

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {

}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {

}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
  self.accessToken = nil;
  if (_typicalVKCompletionBlock) {
    _typicalVKCompletionBlock(authorizationError);
    _typicalVKCompletionBlock = NULL;
  }
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {

}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
  self.accessToken = newToken;
  if (!newToken) {
    return;
  }
  VKRequest *infoRequest = [[VKApi users] get];
  __weak __typeof(self) weakSelf = self;
  [infoRequest executeWithResultBlock:^(VKResponse *response) {
    __strong __typeof(weakSelf) strongSelf = weakSelf;
    if (!strongSelf) {
      return;
    }

    NSString *fname = [response.json[0][@"first_name"] copy];
    NSString *lname = [response.json[0][@"last_name"] copy];
    [[Settings sharedSettings] setupNameAndSave:[NSString stringWithFormat:@"%@ %@", fname, lname]];
    if (strongSelf->_typicalVKCompletionBlock) {
      strongSelf->_typicalVKCompletionBlock(nil);
    }
  }
  errorBlock:^(NSError *error) {
    if (error.code != VK_API_ERROR) {
      [error.vkError.request repeat];
    } else {
      NSLog(@"VK error: %@", error);
      __strong __typeof(weakSelf) strongSelf = weakSelf;
      if (!strongSelf) {
        return;
      }
      strongSelf->_typicalVKCompletionBlock(error.vkError);
    }
  }];
}

#pragma mark - UIApplicationDelegate impl

- (void)applicationWillTerminate:(UIApplication *)application {
  if (self.accessToken && self.isAuthorized) {
    [self.accessToken saveTokenToDefaults:kDefaultsVKTokenKey];
  }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  if (self.accessToken && self.isAuthorized) {
    [self.accessToken saveTokenToDefaults:kDefaultsVKTokenKey];
  }
}


@end