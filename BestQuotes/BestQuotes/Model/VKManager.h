//
// Created by Anton Serov on 23/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^TypicalVKCompletionBlock)(VKError *);

extern NSString * const VKAppId;
extern NSString * const kDefaultsVKTokenKey;

@interface VKManager : NSObject

+ (instancetype)sharedManager;

- (void)authorize:(TypicalVKCompletionBlock)completion;
- (void)signOut:(TypicalVKCompletionBlock)completion;
- (void)likeWithId:(NSString *)id ownerId:(NSString *)ownerId completion:(TypicalVKCompletionBlock)completion;
- (void)unlikeWithId:(NSString *)id ownerId:(NSString *)ownerId completion:(TypicalVKCompletionBlock)completion;
- (void)postOnWall:(NSString *)text completion:(TypicalVKCompletionBlock)completion;
- (void)repostOnWall:(NSString *)text groupId:(NSString *)groupId completion:(TypicalVKCompletionBlock)completion;
- (void)executeTypicalRequest:(VKRequest *)request completion:(TypicalVKCompletionBlock)completion;

- (BOOL)isAuthorized;

@property (nonatomic, readonly, getter=isAuthorized) BOOL authorized;

@end