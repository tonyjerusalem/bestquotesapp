//
// NSLocale(Custom)
// BestQuotes
//

#import "NSLocale+Custom.h"

@implementation NSLocale (Custom)

- (NSString *)shortLocaleIdentifier {
  NSString *localeId = [[NSLocale currentLocale] localeIdentifier];
  if ([localeId rangeOfString:@"_"].location == NSNotFound) return @"ru";
  NSString *locale = [localeId componentsSeparatedByString:@"_"][0];
  return locale;
}

@end