//
// Created by Anton Serov on 07/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "DataManager.h"
#import "NSURL+BQAPI.h"
#import "BQModel.h"
#import "Settings+Additions.h"

NSString * const BQInternetConnectionStatusChanged = @"BQInternetConnectionStatusChanged";

// Use a class extension to expose access to MagicalRecord's private setter methods
@interface NSManagedObjectContext ()
+ (void)MR_setRootSavingContext:(NSManagedObjectContext *)context;
+ (void)MR_setDefaultContext:(NSManagedObjectContext *)moc;
@end

@implementation DataManager

#pragma mark Initialization

+ (instancetype)sharedManager {
  static DataManager *sharedManager;
  if (sharedManager) {
    return sharedManager;
  }

  static dispatch_once_t p;
  dispatch_once(&p, ^{
    sharedManager = [DataManager managerWithBaseURL:[NSURL baseAPIURL]];
    [sharedManager.HTTPClient setDefaultHeader:@"User-Agent"
      value:[NSString stringWithFormat:@"%@/%@ (Mac OS X %@)",
       [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *) kCFBundleExecutableKey] ?:
         [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *) kCFBundleIdentifierKey],
       [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] ?:
         [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *) kCFBundleVersionKey],
       [[NSProcessInfo processInfo] operatingSystemVersionString]]];

    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel MR_defaultManagedObjectModel];
    RKManagedObjectStore *managedObjectStore =
      [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    sharedManager.managedObjectStore = managedObjectStore;
    [managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath =
      [RKApplicationDataDirectory() stringByAppendingPathComponent:@"Base.sqlite"];
    NSString *seedPath =
      [[NSBundle mainBundle] pathForResource:@"Prepopulate" ofType:@"sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore =
      [managedObjectStore addSQLitePersistentStoreAtPath:storePath
        fromSeedDatabaseAtPath:seedPath
        withConfiguration:nil
        options:nil
        error:&error];


    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    [managedObjectStore createManagedObjectContexts];
//    managedObjectStore.managedObjectCache =
//      [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];

    RKEntityMapping *quoteMapping =
      [RKEntityMapping mappingForEntityForName:NSStringFromClass([VKQuote class])
      inManagedObjectStore:managedObjectStore];

    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Base.sqlite"];
    [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:managedObjectStore.persistentStoreCoordinator];
    [NSManagedObjectContext MR_setRootSavingContext:managedObjectStore.persistentStoreManagedObjectContext];
    [NSManagedObjectContext MR_setDefaultContext:managedObjectStore.mainQueueManagedObjectContext];

    [quoteMapping addAttributeMappingsFromDictionary:
      @{
        @"id" : @"postId",
        @"owner_id" : @"ownerId",
        @"date" : @"postDate",
        @"text" : @"text"
      }
    ];
    quoteMapping.identificationAttributes = @[@"postId"];

    RKEntityMapping *likeMapping =
      [RKEntityMapping mappingForEntityForName:NSStringFromClass([Likes class])
      inManagedObjectStore:managedObjectStore];

    [likeMapping addAttributeMappingsFromDictionary:
      @{
        @"can_like" : @"canLike",
        @"can_publish" : @"canPublish",
        @"count" : @"count",
        @"user_likes" : @"userLikes"
      }
    ];

    [quoteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"likes"
      toKeyPath:@"like"
      withMapping:likeMapping]];

    [[RKValueTransformer defaultValueTransformer]
      insertValueTransformer:[RKValueTransformer iso8601TimestampToDateValueTransformer] atIndex:0];


    RKResponseDescriptor *responseDescriptor =
      [RKResponseDescriptor responseDescriptorWithMapping:quoteMapping
        method:RKRequestMethodGET
        pathPattern:nil
        keyPath:@"response.items"
        statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    [sharedManager addResponseDescriptor:responseDescriptor];

    if ([Settings sharedSettings] == nil) {
      [Settings MR_createEntity];
      [sharedManager save];
    }

    [sharedManager.HTTPClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
      [[NSNotificationCenter defaultCenter] postNotificationName:BQInternetConnectionStatusChanged
        object:RKStringFromNetworkReachabilityStatus(status)];
    }];

  });
  return sharedManager;
}

- (void)save {
  NSError *error;
  [self.managedObjectStore.mainQueueManagedObjectContext save:&error];
  if (error) {
    RKLogError(@"Save fault: %@", error);
  }
  [[RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext saveToPersistentStore:&error];
  if (error) {
    RKLogError(@"Save fault: %@", error);
  }
}

- (void)requestQuotesWithType:(RequestQuoteType)type
                      fromNum:(NSInteger)from
                     andCount:(NSInteger)count
                  complection:(loadQuotesCompletion)completion {
  if ((type == DBQuoteRequest || type == FavQuoteRequest) && completion != nil) {
    completion(nil, nil, count);
    return;
  }

  NSDictionary *params = @{
    @"owner_id" : @"-45588871",
    @"filter" : @"owner",
    @"v" : @"5.21",
    @"offset" : @(from),
    @"count" : @(count)
  };

  __weak __typeof(self) weakSelf = self;
  [self getObjectsAtPath:@"wall.get"
    parameters:params
    success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
      if (completion) {
        completion(nil, mappingResult.array, mappingResult.array.count);
      }
    }
    failure:^(RKObjectRequestOperation *operation, NSError *error) {
      __strong __typeof(weakSelf) strongSelf = weakSelf;
      RKLogError(@"Load failed with error: %@", error);
      if (completion) {
        completion(error, nil, 0);
      }
    }
  ];
}

- (void)deleteCachedQuotes {
  NSArray *vkQuotes =
    [VKQuote MR_findAllInContext:[RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext];
  for (VKQuote *q in vkQuotes) {
    [q MR_deleteEntity];
  }
  [self save];
}

- (BOOL)isNetworkAvailable {
  return
    self.HTTPClient.networkReachabilityStatus != AFNetworkReachabilityStatusNotReachable;
}

@end