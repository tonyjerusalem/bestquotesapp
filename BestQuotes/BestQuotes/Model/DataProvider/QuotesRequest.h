//
// Created by Anton Serov on 18/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"

extern CGFloat const BQPortionOfObjectsBeforeLoadingNewBatch;

@protocol DataRequestResultsDelegate;

typedef void(^loadCompletionBlock)(NSError *);

@interface QuotesRequest : NSObject

// If infinite is YES when request executes immediately
- (instancetype)initWithRequestType:(RequestQuoteType)type
                     continuousLoad:(BOOL)infinite
                           delegate:(id <DataRequestResultsDelegate>)delegate;

@property (nonatomic, readonly) RequestQuoteType type;
@property (nonatomic, readonly) BOOL infiniteRequest;
@property (nonatomic) NSUInteger offset;
@property (nonatomic, readonly) NSUInteger count;
@property (nonatomic) NSUInteger numberOfObjectsPerRequest;
@property (nonatomic, weak) id<DataRequestResultsDelegate> delegate;
@property (nonatomic, strong) loadCompletionBlock requestCompletion;

- (BOOL)isSearchMode;

// Load data. If it is first request when offset sets to init value.
// Not working if infiniteRequest is YES
- (void)loadDataWithCompletion:(loadCompletionBlock)completion progressionBlock:(void (^)(double))progression;

- (void)loadDataIfNecessary:(NSUInteger)index;
- (void)loadData;
- (void)cancelDataLoading;

- (NSArray *)searchQuoteWithText:(NSString *)text favorites:(BOOL)favorites;

// Objects accessing
- (id)objectAtIndexedSubscript:(NSUInteger)index;

@end

@protocol DataRequestResultsDelegate
@required
- (void)resultsWillChangeContentInRequest:(QuotesRequest *)request;
- (void)resultsDidChangeContentInRequest:(QuotesRequest *)request;
- (void)resultsChangedInRequest:(QuotesRequest *)request forIndexPaths:(NSArray *)indexPaths;

@optional
-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath;
@end