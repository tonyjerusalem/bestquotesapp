//
// Created by Anton Serov on 02/06/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "BaseTabBarController.h"
#import "QuotesVC.h"


@implementation BaseTabBarController

- (void)viewDidLoad {
  [super viewDidLoad];

  NSArray *tabBarTitles = @[@"Quotes", @"VK Quotes", @"Favorites"];

  NSUInteger index = 0;

  // In fact, it is unstable solution.
  // Once order of view controllers may change.
  // TODO: REMEMBER THIS

  for (UIViewController *uvc in self.viewControllers) {
    if ([uvc isKindOfClass:[QuotesVC class]]) {
      if (index < tabBarTitles.count) {
        ((QuotesVC *)uvc).type = (DisplayQuoteType)index;
        uvc.tabBarItem.title = tabBarTitles[index++];
      }
    }
  }
}

@end