//
//  Author.m
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Author.h"


@implementation Author

@dynamic birthday;
@dynamic deathday;
@dynamic info;
@dynamic name;
@dynamic picture;
@dynamic quotes;
@dynamic vkquotes;

@end
