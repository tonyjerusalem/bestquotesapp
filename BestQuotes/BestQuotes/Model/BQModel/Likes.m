//
//  Likes.m
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "Likes.h"


@implementation Likes

@dynamic canLike;
@dynamic canPublish;
@dynamic count;
@dynamic userLikes;
@dynamic vkquote;

@end
