//
// Created by Anton Serov on 16/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSManagedObject (Additions)

- (NSError *)errorFromOriginalError:(NSError *)originalError error:(NSError *)secondError;

@end