//
//  Likes.h
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VKQuote;

@interface Likes : NSManagedObject

@property(nonatomic, retain) NSNumber *canLike;
@property(nonatomic, retain) NSNumber *canPublish;
@property(nonatomic, retain) NSNumber *count;
@property(nonatomic, retain) NSNumber *userLikes;
@property(nonatomic, retain) VKQuote *vkquote;

@end
