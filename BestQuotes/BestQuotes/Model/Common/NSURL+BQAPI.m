//
// Created by Anton Serov on 07/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "NSURL+BQAPI.h"


@implementation NSURL (BQAPI)

+ (NSURL *)baseAPIURL {
  return [[NSURL alloc] initWithString:@"https://api.vk.com/method"];
}

@end