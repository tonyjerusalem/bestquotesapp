//
// Created by Anton Serov on 24/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Settings.h"

@interface Settings (Additions)

+ (instancetype)sharedSettings;
- (void)setupNameAndSave:(NSString *)name;

@end