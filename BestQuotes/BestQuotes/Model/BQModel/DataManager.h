//
// Created by Anton Serov on 07/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

extern NSString * const BQInternetConnectionStatusChanged;

typedef NS_ENUM(NSInteger, RequestQuoteType) {
  DBQuoteRequest = 0,
  VKQuoteRequest,
  FavQuoteRequest
};

typedef void(^loadQuotesCompletion)(NSError *, NSArray *, NSUInteger);

@interface DataManager : RKObjectManager

+ (instancetype)sharedManager;

- (void)save;

- (void)requestQuotesWithType:(RequestQuoteType)type
                      fromNum:(NSInteger)from
                     andCount:(NSInteger)count
                  complection:(loadQuotesCompletion)completion;

 - (void)deleteCachedQuotes;

- (BOOL)isNetworkAvailable;

@property (nonatomic, readonly, getter=isNetworkAvailable) BOOL networkAvailable;

@end