//
// Created by Anton Serov on 16/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKQuote.h"
#import "QuoteAccessor.h"

extern NSString const *vkQuoteErrorDomain;

@interface VKQuote (Additions) <QuoteAccessor>

@property (nonatomic, readonly) NSString *messageAddress;

@end