//
//  AppDelegate.h
//  BestQuotes
//
//  Created by Anton Serov on 21.08.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) UINavigationController *navController;
@property(strong, nonatomic) UITabBarController *tabController;
@property(nonatomic, readonly) UIStoryboard *mainStoryboard;

- (NSURL *)applicationDocumentsDirectory;

@end
