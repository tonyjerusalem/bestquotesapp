//
// Created by Anton Serov on 20/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Author;

@protocol QuoteAccessor <NSObject>

@property (nonatomic, readonly, getter=isQuoteLike) BOOL quoteLike;

@required
- (NSString *)quoteText;
- (NSString *)formattedQuoteText;
- (BOOL)isQuoteLike;
- (Author *)quoteAuthor;
- (NSDate *)quotePostDate;

- (void)makeLike:(void (^)(void))completion;

@end