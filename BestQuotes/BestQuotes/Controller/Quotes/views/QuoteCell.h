//
// QuoteCell
// BestQuotes
//

#import <Foundation/Foundation.h>

extern const CGFloat delta;
extern const CGFloat defaultLabelHeight;
extern const CGFloat defaultCellHeight;

@protocol QuoteCellDelegate;
@protocol QuoteAccessor;

@interface QuoteCell : UITableViewCell

@property (nonatomic, weak) id<QuoteCellDelegate> delegate;
@property (nonatomic) BOOL expanded;

+ (CGFloat)cellHeightWithText:(NSString *)text andWidth:(CGFloat)width expanded:(BOOL)expanded;

- (IBAction)share:(id)sender;

- (IBAction)favorites:(id)sender;

- (IBAction)showAuthor:(id)sender;

- (void)configureCellWith:(id <QuoteAccessor>)quote expanded:(BOOL)expanded;

@end

@protocol QuoteCellDelegate <NSObject>

@required
- (void)shareQuoteFromCell:(QuoteCell *)cell;

- (void)favoriteQuoteFromCell:(QuoteCell *)cell;

- (void)showAuthorFromCell:(QuoteCell *)cell;

@end