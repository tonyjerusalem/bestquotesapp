//
// Created by Anton Serov on 04/05/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QuoteAccessor;

extern NSString *const BQActivityTypeVK;

@interface VKActivity : UIActivity

@property (nonatomic, strong) id<QuoteAccessor> quote;
@property (nonatomic, strong) UIActivityViewController *controller;


@end