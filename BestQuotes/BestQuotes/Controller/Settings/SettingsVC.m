//
// Created by Anton Serov on 22/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "SettingsVC.h"
#import "Settings.h"
#import "DataManager.h"
#import "VKManager.h"
#import "Settings+Additions.h"


@interface SettingsVC() {

}

@property (nonatomic, weak) IBOutlet UITableViewCell *authorizeCell;
@property (nonatomic, weak) IBOutlet UITableViewCell *quotesPerRequestCell;
@property (nonatomic, weak) IBOutlet UITableViewCell *deleteCacheCell;
@property (nonatomic, weak) IBOutlet UISwitch *cacheQuotesSwitch;

@end


@implementation SettingsVC


#pragma mark - SetGet


#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  [self initializeView];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.tableView reloadData];
  [self updateViews];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
}

#pragma mark - Configure views


- (void)initializeView {
}

- (void)updateViews {
  Settings *settings = [Settings sharedSettings];

  if (settings.userName) {
    self.authorizeCell.textLabel.text = settings.userName;
  } else {
    self.authorizeCell.textLabel.text = @"Авторизироваться";
  }

  self.cacheQuotesSwitch.on = settings.cacheQuotes.boolValue;
  self.quotesPerRequestCell.detailTextLabel.text = settings.quotesPerRequest.stringValue;
}


#pragma mark - Actions


- (IBAction)cacheQuoteSwitchChangeValue:(id)sender {
  Settings *settings = [Settings sharedSettings];

  settings.cacheQuotes = @(((UISwitch *)sender).on);
  [[DataManager sharedManager] save];
}

- (void)authorize {
  __weak __typeof(self) weakSelf = self;

  [[VKManager sharedManager] authorize:^(VKError *error) {
    if (error) {

    } else {
      [weakSelf updateViews];
    }
  }];
}

#pragma mark - UITableViewDelegate impl

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];

  if ([cell.reuseIdentifier isEqualToString:@"ProfileCellIdentifier"]) {
    __weak __typeof(self) weakSelf = self;
    if ([VKManager sharedManager].isAuthorized) {
      [UIActionSheet showFromTabBar:self.tabBarController.tabBar
        withTitle:@"Are you sure want to logout"
        cancelButtonTitle:@"Cancel"
        destructiveButtonTitle:@"Logout"
        otherButtonTitles:nil
        tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
          switch (buttonIndex) {
            case 0:
              [[VKManager sharedManager] signOut:nil];
              [weakSelf updateViews];
              break;
            case 1:
              break;
            default:
              break;
          }
          [tableView deselectRowAtIndexPath:indexPath animated:YES];
      }];
    } else {
      [self authorize];
      [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
  } else if ([cell.reuseIdentifier isEqualToString:@"DeleteVKQuotesCellIdentifier"]) {
    [[DataManager sharedManager] deleteCachedQuotes];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ShowAlert(@"Cache successfully deleted!", @"");
  }
}


#pragma mark - UITableViewDataSource impl


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = nil;
  cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

  return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 2;
}

@end