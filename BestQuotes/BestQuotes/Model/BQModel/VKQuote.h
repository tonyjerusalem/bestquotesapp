//
//  VKQuote.h
//  BestQuotes
//
//  Created by Anton Serov on 25/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Author, Likes;

@interface VKQuote : NSManagedObject

@property (nonatomic, retain) NSDate * postDate;
@property (nonatomic, retain) NSNumber * postId;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSNumber * ownerId;
@property (nonatomic, retain) Author *author;
@property (nonatomic, retain) Likes *like;

@end
