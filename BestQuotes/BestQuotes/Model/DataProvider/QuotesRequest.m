//
// Created by Anton Serov on 18/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "QuotesRequest.h"
#import "VKQuote.h"
#import "Author.h"
#import "NSURL+BQAPI.h"
#import "Quote+Additions.h"
#import "MRNavigationBarProgressView.h"

CGFloat const BQPortionOfObjectsBeforeLoadingNewBatch = 0.7f;

@implementation QuotesRequest {
  RequestQuoteType _type;
  BOOL _infiniteDataLoading;
  BOOL _executingRequest;
  NSMutableArray *_objects;
  MRNavigationBarProgressView *_navigationBarProgressView;
  NSMutableSet *_validObjectsInCurrentRequest;
  BOOL _searchMode;
}

- (instancetype)initWithRequestType:(RequestQuoteType)type
                     continuousLoad:(BOOL)infinite
                           delegate:(id <DataRequestResultsDelegate>)delegate {
  self = [super init];
  if (self) {
    _type = type;
    _offset = 0;
    _numberOfObjectsPerRequest = 20;
    _infiniteDataLoading = infinite;
    self.delegate = delegate;
    _objects = [NSMutableArray array];
    _validObjectsInCurrentRequest = [NSMutableSet set];
    if (_infiniteDataLoading) {
      _navigationBarProgressView =
        [MRNavigationBarProgressView progressViewForNavigationController:appDelegate.navController];
    }
  }
  return self;
}


- (NSString *)debugDescription {
  NSString *format = @"%@\nNumber of objects - %d\n%@";
  NSString *description = [NSString stringWithFormat:format, self, _objects.count, _objects];
  return description;
}


#pragma mark - Indexed Subscript


- (id)objectAtIndexedSubscript:(NSUInteger)index {
  return _objects[index];
}

- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)index {
  // TODO: think later about this
}


#pragma mark - SetGet

- (BOOL)isSearchMode {
  return _searchMode;
}


- (NSUInteger)count {
  return
    _executingRequest ?
      0 :
      _objects.count;
}

- (RequestQuoteType)type {
  return _type;
}

- (BOOL)infiniteRequest {
  return _infiniteDataLoading;
}

#pragma mark - Search

- (NSArray *)searchQuoteWithText:(NSString *)text favorites:(BOOL)favorites {
  if (self.type == VKQuoteRequest ) {
    return nil;
  }

  _searchMode = YES;

  NSString *format;
  NSPredicate *predicate = nil;

  if (favorites) {
    format = @"(text contains[c] %@ OR author.name contains[c] %@) AND favorite = YES";
  }
  else {
    format = @"text contains[c] %@ OR author.name contains[c] %@";
  }

  predicate =
    [NSPredicate predicateWithFormat:format, text ,text];

  NSArray *res =
    [Quote MR_findAllSortedBy:@"text"
      ascending:YES
      withPredicate:predicate];

  _objects = [NSMutableArray arrayWithArray:res];

  return res;
}

#pragma mark - Loading


- (void)loadData {
  if (![DataManager sharedManager].isNetworkAvailable && self.type == VKQuoteRequest) {
    NSError *error =
      [[NSError alloc] initWithDomain:NSURLErrorDomain
        code:NSURLErrorNotConnectedToInternet
        userInfo: nil];
    if (self.requestCompletion) {
      self.requestCompletion(error);
      return;
    }
  }

  if (!_executingRequest) {
    _validObjectsInCurrentRequest = [NSMutableSet set];
  }
  
  _executingRequest = YES;
  
  __weak __typeof(self) weakSelf = self;
  [self loadDataWithCompletion:^(NSError *error) {
    __block NSMutableArray *newIndexPaths = [NSMutableArray array];
    __strong __typeof(weakSelf) strongSelf = weakSelf;
    if (!strongSelf) {
      return;
    }
    if (error) {
      [[NSManagedObjectContext MR_contextForCurrentThread] rollback];
      if (strongSelf.requestCompletion) {
        strongSelf.requestCompletion(error);
      }
      return;
    }
    @synchronized (_objects) {
      Class objectClass = strongSelf.type == VKQuoteRequest ? [VKQuote class] : [Quote class];
      NSString *sortedBy = self.type == VKQuoteRequest ? @"postDate" : @"author.name";
      BOOL ascending = self.type != VKQuoteRequest;
      NSFetchRequest *request =
        [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(objectClass)];
      request.predicate = strongSelf.type == FavQuoteRequest ?
        [NSPredicate predicateWithFormat:@"favorite==YES"] : nil;
      request.fetchLimit = _objects.count + self.numberOfObjectsPerRequest;
      NSSortDescriptor *sortDescriptor =
        [NSSortDescriptor sortDescriptorWithKey:sortedBy ascending:ascending];
      request.sortDescriptors = @[sortDescriptor];
      NSArray *newResult =
        [[NSManagedObjectContext MR_contextForCurrentThread] executeFetchRequest:request error:nil];
      __block NSInteger newRow = _objects.count;
      [newResult enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSUInteger newObjectIndex = [_objects indexOfObject:obj];
        if (newObjectIndex == NSNotFound) {
          [newIndexPaths addObject:[NSIndexPath indexPathForRow:newRow++ inSection:0]];
        }
      }];

      _objects = [NSMutableArray arrayWithArray:newResult];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
      _executingRequest = NO;
      [strongSelf.delegate resultsWillChangeContentInRequest:strongSelf];
      [strongSelf.delegate resultsChangedInRequest:strongSelf forIndexPaths:newIndexPaths];
      [strongSelf.delegate resultsDidChangeContentInRequest:strongSelf];
    });
    
    if (strongSelf.requestCompletion) {
      strongSelf.requestCompletion(error);
    }
  }
  progressionBlock:^(double d) {
    [_navigationBarProgressView setProgress:d animated:YES];
  }];
}

- (void)loadDataWithCompletion:(loadCompletionBlock)completion
    progressionBlock:(void (^)(double))progression {

  __weak __typeof(self) weakSelf = self;
  [[DataManager sharedManager] requestQuotesWithType:self.type
    fromNum:self.offset
    andCount:self.numberOfObjectsPerRequest
    complection:^(NSError *error, NSArray *arrayOfObjects, NSUInteger numberOfObjects) {
      __strong __typeof(weakSelf) strongSelf = weakSelf;
      if (!strongSelf) {
        return;
      }
      if (!completion) {
        return;
      }
      if (error) {
        completion(error);
        [strongSelf cancelDataLoading];
        return;
      }

      strongSelf.offset += numberOfObjects;

      if (weakSelf.type != VKQuoteRequest) {
        if (progression) {
          progression(1.f);
        }
        completion(error);
        return;
      }

      for (VKQuote *quote in arrayOfObjects) {
        if (quote.author) {
          [strongSelf->_validObjectsInCurrentRequest addObject:quote.objectID];
        }
      }

      if (progression) {
        double progress =
          strongSelf->_validObjectsInCurrentRequest.count / strongSelf.numberOfObjectsPerRequest;
        progression(progress);
      }

      if (strongSelf->_validObjectsInCurrentRequest.count >= strongSelf.numberOfObjectsPerRequest) {

        NSInteger numberOfObjectsToDelete =
          strongSelf->_validObjectsInCurrentRequest.count - strongSelf.numberOfObjectsPerRequest;
        NSInteger numberOfDeletedObjects = 0;
        NSManagedObjectContext *context =
          [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
        NSArray *array =
          [VKQuote MR_findAllSortedBy:@"postDate" ascending:YES inContext:context];
        for (VKQuote *quote in array) {
          if (numberOfDeletedObjects == numberOfObjectsToDelete) {
            break;
          }
          if (quote.author == nil) {
            [quote MR_deleteEntity];
            numberOfDeletedObjects++;
          }
        }

        for (VKQuote *quote in array) {
          if (!quote.author) {
            [quote MR_deleteEntity];
          }
        }

        if (completion) {
          completion(error);
        }

      } else {
        [weakSelf loadDataWithCompletion:completion progressionBlock:progression];
        return;
      }
  }];
}

- (void)loadDataIfNecessary:(NSUInteger)index {
  if (_searchMode) {
    return;
  }
  if ((CGFloat)index / _objects.count >= BQPortionOfObjectsBeforeLoadingNewBatch &&
    !_executingRequest) {
    [self loadData];
  }
}

- (void)cancelDataLoading {
  [[DataManager sharedManager] cancelAllObjectRequestOperationsWithMethod:RKRequestMethodAny
    matchingPathPattern:[NSURL baseAPIURL].path];
}

@end