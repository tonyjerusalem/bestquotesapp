//
// Created by Anton Serov on 21/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "BQAppearance.h"

#define DefaultBarColor UIColorFromRGB(225, 255, 230)
#define DefaultBarTextColor UIColorFromRGB(0, 0, 0)

@implementation BQAppearance

+ (void)applyInitialAppearance {
  [[UINavigationBar appearance] setBarTintColor:DefaultBarColor];
  [[UINavigationBar appearance] setTintColor:DefaultBarTextColor];
  [[UITabBar appearance] setBarTintColor:DefaultBarColor];
  [[UITabBar appearance] setTintColor:DefaultBarTextColor];
}

+ (UIColor *)quoteCellHeaderColor {
  return DefaultBarColor;
}

+ (UIColor *)defaultTintColorColor {
  return DefaultBarTextColor;
}

@end