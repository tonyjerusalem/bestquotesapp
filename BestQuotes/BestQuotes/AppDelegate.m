//
//  AppDelegate.m
//  BestQuotes
//
//  Created by Anton Serov on 21.08.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//

#import "DataManager.h"
#import "BQAppearance.h"
#import "Settings.h"
#import "Settings+Additions.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

  [DataManager sharedManager];

  self.navController = [self.mainStoryboard instantiateInitialViewController];
  self.tabController = (UITabBarController *) self.navController.topViewController;
  self.window.rootViewController = self.navController;
  self.window.backgroundColor = [UIColor whiteColor];
  [self.window makeKeyAndVisible];

  NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastClearCacheDate"];
  if (!date) {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastClearCacheDate"];
  } else if ([[NSDate date] timeIntervalSinceDate:date] > 60 * 60 * 24 * 3) { // 3 days
    [[DataManager sharedManager] deleteCachedQuotes];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastClearCacheDate"];
  }

  [BQAppearance applyInitialAppearance];

  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
// Saves changes in the application's managed object context before the application terminates.
  if (![Settings sharedSettings].cacheQuotes.boolValue) {
    [[DataManager sharedManager] deleteCachedQuotes];
  }
}

-(BOOL)application:(UIApplication *)application
           openURL:(NSURL *)url
 sourceApplication:(NSString *)sourceApplication
        annotation:(id)annotation {
  [VKSdk processOpenURL:url fromApplication:sourceApplication];
  return YES;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory {
  return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - SetGet

- (UIStoryboard *)mainStoryboard {
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:UINamed(@"MainUI") bundle:nil];
  return storyboard;
}

@end
