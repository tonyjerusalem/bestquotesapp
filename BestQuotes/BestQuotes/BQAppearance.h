//
// Created by Anton Serov on 21/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BQAppearance : NSObject

+ (void)applyInitialAppearance;
+ (UIColor *)quoteCellHeaderColor;
+ (UIColor *)defaultTintColorColor;

@end