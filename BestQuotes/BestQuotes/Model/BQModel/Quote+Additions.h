//
// Created by Anton Serov on 20/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quote.h"
#import "QuoteAccessor.h"

@interface Quote (Additions) <QuoteAccessor>
@end