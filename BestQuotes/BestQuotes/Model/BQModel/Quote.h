//
//  Quote.h
//  BestQuotes
//
//  Created by Anton Serov on 14/04/14.
//  Copyright (c) 2014 hexmaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Author;

@interface Quote : NSManagedObject

@property(nonatomic, retain) NSNumber *favorite;
@property(nonatomic, retain) NSString *text;
@property(nonatomic, retain) Author *author;

@end
