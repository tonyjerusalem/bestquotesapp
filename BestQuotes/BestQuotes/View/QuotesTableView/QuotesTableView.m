//
// QuotesTableView
// BestQuotes
//

#import "QuotesTableView.h"

@interface QuotesTableView ()
@property(nonatomic, weak) IBOutlet UIView *unavailableView;
@property(nonatomic, strong) UILabel *textLabel;
@end

@implementation QuotesTableView {
}

- (void)setAvailiable:(BOOL)availiable {
  __weak __typeof (self) weakSelf = self;
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_availiable == availiable) return;
    _availiable = availiable;
    if (!_availiable) {
      if ([weakSelf.subviews containsObject:weakSelf.unavailableView])
        return;
      [weakSelf addSubview:weakSelf.unavailableView];
      [weakSelf bringSubviewToFront:weakSelf.unavailableView];
      weakSelf.scrollEnabled = NO;
    } else {
      [weakSelf.unavailableView removeFromSuperview];
      weakSelf.scrollEnabled = YES;
    }
  });
}

@end