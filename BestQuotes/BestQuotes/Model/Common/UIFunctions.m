//
//  UIFunctions.m
//  BestQuotes
//
//  Created by Anton Serov on 21.08.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//


NSString *UINamed(NSString *resource) {
  UIDevice *device = [UIDevice currentDevice];
  NSString *extension = [resource pathExtension];
  NSString *name = [resource stringByDeletingPathExtension];
  NSString *prefix = nil;
  switch (device.userInterfaceIdiom) {
    case UIUserInterfaceIdiomPhone:
      // iPhone
      prefix = @"iphone";
      break;
    case UIUserInterfaceIdiomPad:
      // iPad
      prefix = @"ipad";
      break;
    default:
      break;
      // unknown idiom
  }
  if (prefix) {
    if (extension.length) {
      return [NSString stringWithFormat:@"%@_%@.%@", name, prefix, extension];
    }
    return [NSString stringWithFormat:@"%@_%@", name, prefix];
  } else {
    return resource;
  }

}