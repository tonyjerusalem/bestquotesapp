//
// Created by Anton Serov on 16/04/14.
// Copyright (c) 2014 hexmaster. All rights reserved.
//

#import "VKQuote+Additions.h"
#import "NSString+Utils.h"
#import "Likes.h"
#import "Author.h"
#import "VKManager.h"
#import "DataManager.h"

NSString const *vkQuoteErrorDomain = @"vkQuoteErrorDomain";

@implementation VKQuote (Additions)

- (NSString *)messageAddress {
  return [NSString stringWithFormat:@"wall%@_%@", self.ownerId, self.postId];
}

- (void)setText:(NSString *)text {
  NSArray *split = [text componentsSeparatedByString:@"©"];
  NSString *quoteText = split.count == 2 ? split[0] : text;
  quoteText = quoteText.stringByStrippingHTML.trimmedString;
  NSString *authorText = split.count == 2 ? split[1] : nil;
  authorText = authorText.stringByStrippingHTML.trimmedString;
  authorText = [authorText componentsSeparatedByString:@","][0];
  Author *author = [Author MR_findFirstByAttribute:@"name"
    withValue:authorText
    inContext:self.managedObjectContext];
  if (author) {
    if (author.managedObjectContext == self.managedObjectContext) {
      self.author = author;
    } else {
      Log(@"Diff context's");
    }
  }
  [self willChangeValueForKey:@"text"];
  [self setPrimitiveValue:quoteText forKey:@"text"];
  [self didChangeValueForKey:@"text"];
}

#pragma mark - QuoteAccessor impl


- (NSString *)quoteText {
  return self.text;
}

- (NSString *)formattedQuoteText {
  if (!self.author) {
    return self.quoteText;
  } else {
    NSString *ft = [NSString stringWithFormat:@"%@\n%@", self.quoteText, self.author.name];
    return ft;
  }
}

- (BOOL)isQuoteLike {
  return self.like.userLikes.boolValue;
}

- (Author *)quoteAuthor {
  return self.author;
}


- (NSDate *)quotePostDate {
  return self.postDate;
}

- (void)makeLike:(void (^)(void))completion {
  void (^likeUnlikeBlock)() = ^{
    if (self.like.userLikes.boolValue) {
      __weak __typeof(self) weakSelf = self;
      [[VKManager sharedManager] unlikeWithId:self.postId.stringValue
        ownerId:self.ownerId.stringValue
        completion:^(VKError *error) {
          if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
              weakSelf.like.userLikes = @(NO);
              [[DataManager sharedManager] save];
              if (completion) {
                completion();
              }
            });
            return;
          } else {
            ShowAlert(@"Error", error.errorReason);
          }
        }];
    } else {
      __weak __typeof(self) weakSelf = self;
      [[VKManager sharedManager] likeWithId:self.postId.stringValue
        ownerId:self.ownerId.stringValue
        completion:^(VKError *error) {
          if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
              weakSelf.like.userLikes = @(YES);
              [[DataManager sharedManager] save];
              completion();
            });
            return;
          } else {
            ShowAlert(@"Error", error.errorReason);
          }
        }];
    }
  };

  if ([VKManager sharedManager].authorized) {
    likeUnlikeBlock();
  } else {
    [UIAlertView showWithTitle:@"Authorization"
      message:@"You not authorized in VK. Would you like to log in?"
      cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Log in"]
      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
          [[VKManager sharedManager] authorize:^(VKError *error) {
            if (error) {
              ShowAlert(@"Error in authorization", error.errorReason);
            } else {
              likeUnlikeBlock();
            }
          }];
        }
    }];
  }
}


@end