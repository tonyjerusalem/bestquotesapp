//
//  QuotesVC.m
//  BestQuotes
//
//  Created by Anton Serov on 18.09.13.
//  Copyright (c) 2013 hexmaster. All rights reserved.
//

#import "QuotesVC.h"
#import "QuoteCell.h"
#import "QuotesRequest.h"
#import "QuoteAccessor.h"
#import "Author.h"
#import "AuthorVC.h"
#import "Settings.h"
#import "Settings+Additions.h"
#import "VKActivity.h"
#import "VKManager.h"
#import "BQAppearance.h"

@interface QuotesVC()<QuoteCellDelegate, DataRequestResultsDelegate, UISearchBarDelegate> {
  NSIndexPath *_expandedIndexPath;
}

@property (nonatomic, strong) UIBarButtonItem *rightBarButton;
@property (nonatomic, strong) QuotesRequest *quotesRequest;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;

@end

@implementation QuotesVC

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
  }
  return self;
}

- (void)configureViews {
  switch (self.type) {
    case DisplayDBQuoteType:
      self.title = NSLocalizedString(@"Quotes", @"");
      self.rightBarButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
          target:self action:@selector(search:)];
      break;
    case DisplayVKQuoteType:
      self.title = NSLocalizedString(@"New Quotes", @"");
      self.rightBarButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
          target:self action:@selector(refresh:)];
      break;
    case DisplayFavQuoteType:
      self.title = NSLocalizedString(@"Favorites", @"");
      self.rightBarButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
          target:self action:@selector(search:)];
      break;
  }
  appDelegate.tabController.title = self.title;
  appDelegate.tabController.navigationItem.rightBarButtonItem = self.rightBarButton;

  self.searchBar.tintColor = [BQAppearance defaultTintColorColor];
}

- (void)setupQuotesRequest {
  self.quotesRequest =
    [[QuotesRequest alloc]
      initWithRequestType:(RequestQuoteType)self.type
      continuousLoad:YES
      delegate:self];

  self.quotesRequest.requestCompletion = ^(NSError *error) {
      if (error) {
        [UIAlertView showWithTitle:@"Error"
          message:error.localizedDescription
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil
          tapBlock:nil];
      }
  };

  self.quotesRequest.numberOfObjectsPerRequest =
    [Settings sharedSettings].quotesPerRequest.unsignedIntegerValue;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  NSLog(@"%@", self);

  [self setupQuotesRequest];

  [self.quotesRequest loadData];

  NSLog(@"%@", self.tableView);
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  [self configureViews];
  self.quotesRequest.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.quotesRequest.delegate = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [self.quotesRequest cancelDataLoading];
}


#pragma mark - Segues


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
  if ([segue.identifier isEqualToString:@""] && indexPath) {
    // TODO: what's happened here
  }
}

#pragma mark - Properties

- (void)setType:(DisplayQuoteType)type {
  if (_type == type) {
    _type = type;
  }

//  if ([self isViewLoaded]) {
//    self.quotesRequest = nil;
//    [self.tableView reloadData];
//    [self setupQuotesRequest];
//  }

  _type = type;
}


#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row < self.quotesRequest.count) {
    BOOL expanded =
      [indexPath compare:_expandedIndexPath] == NSOrderedSame;
    id<QuoteAccessor> quote = self.quotesRequest[indexPath.row];
    CGFloat height = [QuoteCell cellHeightWithText:quote.quoteText
                                andWidth:280.f
                                expanded:expanded];
    return height;
  } else {
    return 40;
  }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.quotesRequest.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
                     cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"QuoteCell";
  QuoteCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  BOOL expanded =
    [indexPath compare:_expandedIndexPath] == NSOrderedSame;
  id<QuoteAccessor> quote = self.quotesRequest[indexPath.row];

  [cell configureCellWith:quote expanded:expanded];
  cell.delegate = self;
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  [self.quotesRequest loadDataIfNecessary:indexPath.row];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row < self.quotesRequest.count) {
    [self showQuoteDetails:self.quotesRequest[indexPath.row]];
  }
}


#pragma mark - Actions

- (void)search:(id)sender {
  self.searchBar.frame = CGRectMake(0, 0, 0, 40);
  self.searchBar.delegate = self;
  if (!appDelegate.tabController.navigationItem.titleView) {
    appDelegate.tabController.navigationItem.titleView =
      [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 40)];
  }
  [appDelegate.tabController.navigationItem.titleView addSubview:self.searchBar];
  [UIView animateWithDuration:0.2f animations:^{
    appDelegate.tabController.navigationItem.rightBarButtonItem = nil;
    self.searchBar.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame) - 10, 40);
  }
  completion:^(BOOL finished) {
    [self.searchBar becomeFirstResponder];
  }];
}

- (void)refresh:(id)sender {
  [self.quotesRequest cancelDataLoading];
  self.quotesRequest = nil;
  [self.tableView reloadData];
  [self setupQuotesRequest];
  [self.quotesRequest loadData];
}


- (void)shareTappedAtIndex:(NSIndexPath *)indexPath {
  if (!indexPath)
    return;

  id<QuoteAccessor> quote = self.quotesRequest[indexPath.row];

  VKActivity *vkActivity = [[VKActivity alloc] init];

  BOOL canShareVK =
    [DataManager sharedManager].isNetworkAvailable && [VKManager sharedManager].isAuthorized;

  NSArray *customActivities = canShareVK ?
    @[vkActivity] : nil;

  UIActivityViewController *shareActivityVC =
    [[UIActivityViewController alloc] initWithActivityItems:@[quote.formattedQuoteText]
                                      applicationActivities:customActivities];
  vkActivity.controller = shareActivityVC;
  vkActivity.quote = quote;
  [self presentViewController:shareActivityVC animated:YES completion:nil];
}

- (void)favoritesTappedAtIndex:(NSIndexPath *)indexPath {
  if (!indexPath)
    return;

  id<QuoteAccessor> quote = self.quotesRequest[indexPath.row];

  __weak __typeof(self) weakSelf = self;
  [quote makeLike:^{
    [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath]
                        withRowAnimation:UITableViewRowAnimationAutomatic];
  }];

}

- (void)showQuoteDetails:(id<QuoteAccessor>)quote {
  if ([self.tableView.indexPathForSelectedRow compare:_expandedIndexPath] == NSOrderedSame) {
    _expandedIndexPath = nil;
  } else {
    _expandedIndexPath = self.tableView.indexPathForSelectedRow;
  }

  [self.tableView reloadRowsAtIndexPaths:@[self.tableView.indexPathForSelectedRow]
                  withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - DataRequestResultsDelegate impl


- (void)resultsWillChangeContentInRequest:(QuotesRequest *)request {
  [self.tableView beginUpdates];
}

- (void)resultsDidChangeContentInRequest:(QuotesRequest *)request {
  [self.tableView endUpdates];
}

- (void)resultsChangedInRequest:(QuotesRequest *)request forIndexPaths:(NSArray *)indexPaths {
//  NSLog(@"%@", indexPaths);
  [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - QuoteCellDelegate impl


- (void)shareQuoteFromCell:(QuoteCell *)cell {
  [self shareTappedAtIndex:[self.tableView indexPathForCell:cell]];
}

- (void)favoriteQuoteFromCell:(QuoteCell *)cell {
  [self favoritesTappedAtIndex:[self.tableView indexPathForCell:cell]];
}

- (void)showAuthorFromCell:(QuoteCell *)cell {
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  AuthorVC *vc = [appDelegate.mainStoryboard instantiateViewControllerWithIdentifier:@"AuthorVCID"];
  id<QuoteAccessor> q = self.quotesRequest[indexPath.row];
  vc.author = q.quoteAuthor;
  [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
  [UIView animateWithDuration:0.2f animations:^{
    [self.searchBar resignFirstResponder];
    self.searchBar.frame = CGRectMake(0, 0, 0, 40);
    appDelegate.tabController.navigationItem.titleView = nil;
  }
  completion:^(BOOL finished) {
    [self.searchBar removeFromSuperview];
    self.searchBar.text = @"";
    [self configureViews];

    if (self.quotesRequest.isSearchMode) {
      self.quotesRequest = nil;
      [self.tableView reloadData];
      [self setupQuotesRequest];
      [self.quotesRequest loadData];
    }

  }];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
  if (searchText.length == 0) {
    return;
  }
  [self.quotesRequest searchQuoteWithText:searchText favorites:self.type == DisplayFavQuoteType];
  [self.tableView reloadData];
}

@end
