#import "Common.h"
#import <AFNetworking/AFNetworking.h>

typedef NS_ENUM(NSInteger, DataProviderType) {
  CoreDataProvider = 0,
  VKDataProvider
};